import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  // final vs const: 
  // final can be initialized and it never changes (run time constant)
  // const must be given a value when created (compiled time constant)
  final Function selectHandler;
  final String answerText;

  Answer(this.selectHandler, this.answerText);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        child: Text(answerText),
        onPressed: selectHandler,
        color: Colors.blue,
        textColor: Colors.white,
      ),
    );
  }
}
